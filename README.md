# Pixyz Plugin for Unity

[Documentation 2021.1](https://www.pixyz-software.com/documentations/html/2021.1/plugin4unity/index.html)

## Installation
The plugin can be installed using the .unitypackage or from the Package Manager.  
Use the Package Manager if you are using the plugin in several projects or if you want to keep the plugin outside of your project.

### From .unitypackage
* Download the .unitypackage from the [website download page](https://www.pixyz-software.com/download/)
* Double click on the downloaded .unitypackage to install the content in your current Unity project

### From Package Manager
Open the Package Manager in Unity from Windows > Package Manager.  
The plugin can then be install via git or local

#### With git *(git must be installed)*
* In the Unity Package Manager, click + > Add package from git URL...
* Paste in `https://gitlab.com/pixyz/samples/plugin-unity/plugin-unity.git`
* Wait until the package is installed (there are no progress bar while git pulls the plugin)

#### With .zip (local)
* Download the latest version from gitlab in .zip
* Extract .zip in folder
* In the Unity Package Manager, click + > Add package from disk...
* Select the package.json that is in the folder you just created
* Wait until the package is installed
